var express = require('express');
var mongoose = require("mongoose");
var router = express.Router();

mongoose.connect('mongodb://localhost/Telecomunication', { useNewUrlParser: true, useUnifiedTopology: true })

var logsSchema = new mongoose.Schema({
    loadClass: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    searchClasspath: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    searchFile: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    replaceExtendedMetaClasses: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    ViewRepositoryClientImpl: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    ViewRepositoryImpl: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    WebJARS: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    startDate: String,
    endData: String
});

var logsModel = mongoose.model('411W9', logsSchema)

router.get('/', function (req, res, next) {
    mongoose.model('411W9').find(function (err, logs) {
        res.send(logs);
    })
});

module.exports = router;
